package ru.swiftail.starset.web

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue
import io.micronaut.views.View
import org.apache.commons.lang3.StringUtils
import org.jetbrains.exposed.sql.transactions.transaction
import ru.swiftail.starset.db.DTOFetch
import ru.swiftail.starset.db.DTOSiteFetch
import ru.swiftail.starset.db.Db

@Controller("/starset")
class StarsetController {

    private fun getAppFetches() = transaction {
        Db.AppFetch.all().map { df ->
            DTOFetch(df.id, df.checksum, df.signature, df.fetchDate)
        }
    }

    private fun getSiteFetches() = transaction {
        Db.SiteFetch.all().map { df ->
            DTOSiteFetch(df.id, df.checksum, df.signature, df.fetchDate, df.scripts)
        }
    }

    @View("starset")
    @Get("/")
    @Produces(MediaType.TEXT_HTML)
    fun index(): HttpResponse<Any> {
        return HttpResponse.ok(mapOf<String, Any>(
                "appFetches" to getAppFetches(),
                "siteFetches" to getSiteFetches()
        ))
    }

    @Get("/raw")
    @Produces(MediaType.TEXT_PLAIN)
    fun raw(@QueryValue(value = "id") id: Int): HttpResponse<String> = try {
        HttpResponse.ok(transaction {
            Db.AppFetch[id].data
        })
    } catch (e: Exception) {
        HttpResponse.notFound()
    }

    @Get("/script")
    @View("script")
    @Produces(MediaType.TEXT_HTML)
    fun script(@QueryValue(value = "id") id: Int): HttpResponse<Any> {
        val data = try {
            transaction {
                Db.AppFetch[id].data
            }
        } catch (e: Exception) {
            return HttpResponse.notFound()
        }
        return HttpResponse.ok(mapOf<String, Any>("data" to data, "id" to id))
    }

    @Get("/diff")
    @Produces(MediaType.TEXT_HTML)
    fun diff(@QueryValue(value = "id1") id1: Int, @QueryValue(value = "id2") id2: Int): HttpResponse<String> {

        val (data1, data2) = try {
            transaction {
                val data1 = Db.AppFetch[id1].data
                val data2 = Db.AppFetch[id2].data
                data1 to data2
            }
        } catch (e: Exception) {
            return HttpResponse.notFound()
        }

        val diff1 = StringUtils.difference(data1, data2)
        val diff2 = StringUtils.difference(data2, data1)

        //language=HTML
        return HttpResponse.ok("""
            <pre>
            <b>Difference between fetch <a href='/starset/raw?id=$id1'>#$id1</a> and <a 
            href='/starset/raw?id=$id2'>#$id2:</a></b>
            <b style='color: lightseagreen'>Added (in #$id1):</b>
            %diff2
            <b style='color: red'>Removed:</b>
            %diff1
            </pre>
        """
                .trimIndent()
                .replace("%diff2", diff2)
                .replace("%diff1", diff1))
    }
}