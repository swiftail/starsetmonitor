package ru.swiftail.starset.fetcher

import org.slf4j.LoggerFactory
import java.net.URL

class Fetcher(address: String) {

    companion object {
        private val logger = LoggerFactory.getLogger(Fetcher::class.java)
    }

    private val url = URL(address)

    fun fetch(): ByteArray {
        logger.info("Fetching $url")
        return url.readBytes()
    }
}