package ru.swiftail.starset.db

import org.jetbrains.exposed.dao.EntityID
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

open class DTOFetch(val id: EntityID<Int>, val checksum: Long, val signature: String, val fetchDate: DateTime) {

    @JvmOverloads
    fun formatFetchDate(fetchDate: DateTime = this.fetchDate): String {
        return fetchDate.withZone(DateTimeZone.UTC).toString("dd.MM HH:mm")
    }

    @JvmOverloads
    fun formatChecksum(checksum: Long = this.checksum): String {
        return java.lang.Long.toHexString(checksum)
    }

}

class DTOSiteFetch(
        id: EntityID<Int>, checksum: Long, signature: String, fetchDate: DateTime, val content: String
) : DTOFetch(id, checksum, signature, fetchDate)