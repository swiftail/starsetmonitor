package ru.swiftail.starset.db

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

object Db {
    public object AppFetches : IntIdTable("DataFetches") {

        val data = text("data")
        val checksum = long("checksum")
        val fetchDate = datetime("fetch_date")
        val signature = varchar("signature", 20 * 3)
    }

    public class AppFetch(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<AppFetch>(AppFetches)

        var data by AppFetches.data
        var checksum by AppFetches.checksum
        var signature by AppFetches.signature
        var fetchDate by AppFetches.fetchDate
    }

    public object SiteFetches : IntIdTable("SiteFetches") {
        val scripts = text("scripts")
        val checksum = long("checksum")
        val fetchDate = datetime("fetch_date")
        val signature = varchar("signature", 20 * 3)
    }

    public class SiteFetch(id: EntityID<Int>): IntEntity(id) {
        companion object : IntEntityClass<SiteFetch>(SiteFetches)

        var scripts by SiteFetches.scripts
        var checksum by SiteFetches.checksum
        var signature by SiteFetches.signature
        var fetchDate by SiteFetches.fetchDate
    }

    fun init() {
        transaction {
            SchemaUtils.create(AppFetches, SiteFetches)
        }
    }
}