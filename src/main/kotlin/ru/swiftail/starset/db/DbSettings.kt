package ru.swiftail.starset.db

import org.jetbrains.exposed.sql.Database
import org.slf4j.LoggerFactory

object DbSettings {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    private const val url = "jdbc:sqlite:./db/starset.db"

    val db by lazy {
        logger.info("Connecting as $url")
        Database.connect(url, "org.sqlite.JDBC")
    }
}