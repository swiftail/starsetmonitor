package ru.swiftail.starset.util

import io.micronaut.context.event.ApplicationEventListener
import io.micronaut.discovery.event.ServiceStartedEvent
import ru.swiftail.starset.db.Db
import ru.swiftail.starset.db.DbSettings
import javax.inject.Singleton

@Singleton
class StarsetLoader : ApplicationEventListener<ServiceStartedEvent> {
    override fun onApplicationEvent(event: ServiceStartedEvent) {
        DbSettings.db
        Db.init()
        Discord.init()
    }
}