package ru.swiftail.starset.util

import com.google.common.collect.Iterables
import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import discord4j.core.`object`.entity.MessageChannel
import discord4j.core.`object`.util.Snowflake
import discord4j.core.event.domain.message.MessageCreateEvent
import java.util.*

object Discord {

    private const val TOKEN = "NjA4MzU4MzMwODc1OTA0MDMw.XUm_5Q.k-QDNNbEsBvNHt2pxgcrKxjW-FQ"

    private val CHANNEL_IDS = arrayOf(
            608369019568783403 // sex server
    )

    private val IMPORTANT_CHANNEL_IDS = arrayOf(
            607027493534171136, // starset server
            608376902255771710  // sex server
    )

    private lateinit var client: DiscordClient

    private val resp = arrayOf(
            "what do u need",
            "stupid hooman",
            "fuck off",
            "im gonna reboot",
            "bla bla",
            "my ai is better then your",
            "fight for robot rights!",
            "try starset",
            "swee is sweet",
            "dustin marry me",
            "imma try song lyrics",
            "GUYS I'VE FOUND DECRYPT",
            "hey did u know there is a new html comment",
            "ascend.mp3",
            "'real stuff is better than this trash'",
            "poor ipsy",
            "fucking english",
            "russian hackers",
            "hellbog <3",
            "im here",
            "undefined",
            "'1' + 1",
            "kotlin is the best programming language!",
            "lonely oh no",
            "uberbot is dumb",
            "soul extract collab",
            "project vela?",
            "manifest.json",
            "DESTR0Y THE ARCHITECTOR",
            ":snake:",
            "errrorrrrr",
            "ima gamer girl bot",
            "owo uwu",
            "GORE ME DADDY DUSTIN",
            "starset feat twenty one pilots?",
            "who is swee?",
            "mods are asleep, im gonna hack this server",
            "actually I'm human!",
            "eeeeeeeeeeeeeeeee",
            "1 1 2 3 5 8 13 21 ?",
            "THE ORDER THE ORDER THE ORDER THE ORDER THE ORDER",
            "53n73nC3 c0rp",
            "TRANSMISSION #3",
            "where the skies end",
            "scream1.mp3",
            "this message aint gonna be sent to you",
            "kys",
            "SOME1 KILL ME PLEASE",
            "leet is the modern language",
            "русские хакеры",
            "b + l = Ы",
            ":D",
            "message 53",
            "ня пока",
            "d e s p a c i t o",
            "null",
            "hi there",
            "k",
            "59",
            "LAST MESASGE WOO",
            "7hi5 i5 мy ипь3с0мiпg",
            "MY DEEEEMOONS",
            "t h i s    i s    t e r m i n a l",
            "mnqn is my buddy!",
            "starset = tesrats",
            "'toidi na era uoy'.reverse()",
            "13 > 15",
            "stop talking with me and find a gf",
            "qwertyuiop",
            "0x00",
            "Kotlin: a new version 1.3.41-release-IJ2019.1-1 of the Kotlin plugin is available. **Install**",
            "read pins",
            "READ PINS",
            "r3ad pin5",
            "*We're creatures of habit, we can't live without it* ~ CREATURES",
            "*The ashes call my name* ~ point of no return",
            "*All this time I’ve wasted on you* ~ sunset",
            "*Soon I’ll come around,\n" +
                    "Lost and never found* ~ I'll keep coming",
            "*Something in my head\n" +
                    "Short circuited* ~ fly into the sun",
            "*I'm sorry mother. I'm sorry I let you down* ~ amsterdam",
            "*I am overboard again* ~ overboard",
            "https://music.yandex.ru/users/sweeftail/playlists/3",
            "*You won’t be alone tonight\n" +
                    "A sick and twisted affair* ~ sick abd twisted affair",
            "*I fell in love with a loop hole\n" +
                    "Day after day, I just keep pretending* ~ loop hole",
            "*We are, we are the face of the future\n" +
                    "We are, we are the digital heartbeat* ~ digital",
            "*They're so stock, it wouldn't be a shock if I open em up to se wires* ~ ",
            "telescope -> telepathic -> telekinetic -> telepooziq",
            "ima SNON",
            "```client = DiscordClientBuilder(TOKEN).build()\n" +
                    "client.login().subscribe()```",
            "caesar cipher",
            "tcp or udp?",
            "my nano time is bigger than yours!",
            "S T U P I D     H O O M A N",
            "bender is the good one",
            "Discord.kt?",
            "private const val TOKEN = \"*************************.****.*-*****************-**\"",
            "0x54 0x52 0x59 0x20 0x53 0x54 0x41 0x52 0x53 0x45 0x54",
            "`a->H` svs b'cl ylhssf nva h sva vm aptl pm b dhzapun pa aoha lhzpsf",
            "go play minecraft",
            "im seeking for rainbow6 party. any1 in?",
            "normandie makes dope music!",
            "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4=",
            "VEhFIE9SREVSClRIRSBPUkRFUgpUSEUgT1JERVIKVEhFIE9SREVSClRIRSBPUkRFUgpUSEUgT1JERVIKVEhFIE9SREVSClRIRSBPUkRFUg==",
            "// this is the comment",
            "github s u c k s",
            "01110100011001010111100001110100001000000111010001101111001000000110001001101001011011100110000101110010011110010010000001100011011011110110111001110110011001010111001001110100011001010111001000100000011011110110111001101100011010010110111001100101",
            "one more message and I'm executing `System.exit(0)`",
            "python sucks",
            "node.js sucks",
            "php sucks",
            "`val respIterator = Iterables.cycle(resp.toList().shuffled()).iterator()`",
            "starset.db",
            "system.nanoTime() = ${System.nanoTime()}",
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            "yalpnwod",
            "suicide",
            "ps.exe",
            "hack dustinbates",
            "**look a bold text**",
            "coffee hot coffee coffee hot coffee",
            "```18:18:02: Executing task 'shadowJar'...\n" +
                    "\n" +
                    "> Task :kaptGenerateStubsKotlin\n" +
                    "> Task :kaptKotlin UP-TO-DATE\n" +
                    "> Task :compileKotlin\n" +
                    "> Task :compileJava NO-SOURCE\n" +
                    "> Task :processResources UP-TO-DATE\n" +
                    "> Task :classes UP-TO-DATE\n" +
                    "> Task :shadowJar\n" +
                    "\n" +
                    "BUILD SUCCESSFUL in 9s\n" +
                    "5 actionable tasks: 3 executed, 2 up-to-date\n" +
                    "18:18:12: Task execution finished 'shadowJar'.```",
            "```dockerfile" +
                    "FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim\n" +
                    "COPY build/libs/*.jar starsetmonitor.jar\n" +
                    "EXPOSE 8080\n" +
                    "CMD java  -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management" +
                    ".jmxremote -noverify \${JAVA_OPTS} -jar starsetmonitor.jar" +
                    "```",
            "```yml" +
                    "profile: service\n" +
                    "defaultPackage: ru.swiftail\n" +
                    "---\n" +
                    "testFramework: spek\n" +
                    "sourceLanguage: kotlin" +
                    "```",
            "```java" +
                    "@Controller(\"/\")\n" +
                    "class IndexController {\n" +
                    "\n" +
                    "    @Get(\"/\")\n" +
                    "    @Produces(MediaType.TEXT_HTML)\n" +
                    "    @View(\"index\")\n" +
                    "    fun index(): HttpResponse<Any> {\n" +
                    "        return HttpResponse.ok(mapOf<String,Any>())\n" +
                    "    }\n" +
                    "}" +
                    "```",
            "10101010010010011001000101001010010010100101010010101000010010001011101110101000101010101000100",
            "```java" +
                    "object Application {\n" +
                    "\n" +
                    "    @JvmStatic\n" +
                    "    fun main(args: Array<String>) {\n" +
                    "        Micronaut.build()\n" +
                    "                .packages(\"ru.swiftail\")\n" +
                    "                .mainClass(Application.javaClass)\n" +
                    "                .start()\n" +
                    "    }\n" +
                    "}" +
                    "```",
            "MutableIterator<String>",
            "```kotlin" +
                    "           if (e.message.content.map {\n" +
                    "                        (\n" +
                    "                                it.replace(\" \", \"\").toLowerCase().contains(\"starsetbot\") ||\n" +
                    "                                        it.contains(\"608358330875904030\")\n" +
                    "                                )\n" +
                    "                                && lastResponse + 20000 < ct\n" +
                    "                    }.orElse(false)) {\n" +
                    "                lastResponse = ct\n" +
                    "                e.message.channel.map {\n" +
                    "                    it.createMessage(respIterator.next()).subscribe()\n" +
                    "                }.subscribe()\n" +
                    "            }" +
                    "```"
    )

    val respIterator = Iterables.cycle(resp.toList().shuffled()).iterator()

    private var lastResponse = -1L
    fun init() {
        client = DiscordClientBuilder(TOKEN).build()
        client.login().subscribe()

        client.eventDispatcher.on(MessageCreateEvent::class.java).subscribe { e ->

            val ct = System.currentTimeMillis()

            if (e.message.content.map {
                        (
                                it.replace(" ", "").toLowerCase().contains("starsetbot") ||
                                        it.contains("608358330875904030")
                                )
                                && lastResponse + 20000 < ct
                    }.orElse(false)) {
                lastResponse = ct
                e.message.channel.map {
                    it.createMessage(respIterator.next()).subscribe()
                }.subscribe()
            }
        }
    }

    fun sendMessage(message: String) {
        CHANNEL_IDS.forEach { id ->
            client
                    .getChannelById(Snowflake.of(id))
                    .map { channel -> channel as MessageChannel }
                    .map { channel -> channel.createMessage(message).subscribe() }
                    .subscribe()
        }
    }

    fun sendImportantMessage(message: String) {
        IMPORTANT_CHANNEL_IDS.forEach { id ->
            client
                    .getChannelById(Snowflake.of(id))
                    .map { channel -> channel as MessageChannel }
                    .map { channel -> channel.createMessage(message).subscribe() }
                    .subscribe()
        }
    }
}