package ru.swiftail.starset

import java.math.BigInteger
import java.util.*
import java.util.zip.CRC32

object Checksum {
    private val digest = CRC32()

    fun get(data: ByteArray): Long {
        digest.reset()
        digest.update(data)
        return digest.value
    }
}