package ru.swiftail.starset.schedule

import io.micronaut.scheduling.annotation.Scheduled
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.jsoup.Jsoup
import org.slf4j.LoggerFactory
import ru.swiftail.starset.Checksum
import ru.swiftail.starset.db.Db
import ru.swiftail.starset.fetcher.Fetcher
import ru.swiftail.starset.util.Discord
import ru.swiftail.starset.util.SignatureGenerator
import java.net.URL
import javax.inject.Singleton

@Singleton
class FetchScheduler {

    private val appFetcher = Fetcher("https://starsetonline.com/js/app.js")
    private val siteFetcher = Fetcher("https://starsetonline.com/")

    companion object {
        private val logger = LoggerFactory.getLogger(FetchScheduler::class.java)
    }

    private fun generateSignature(seed: Long): String {
        return SignatureGenerator.generate(seed, 3)
    }


    private fun fetchApp() {
        val prev = transaction {
            val one = Db.AppFetch.all().orderBy(Db.AppFetches.id to SortOrder.DESC).limit(1)
            if(one.empty()) null else one.toList()[0]
        }

        val bytes = appFetcher.fetch()


        val checksum = Checksum.get(bytes)
        val signature = generateSignature(checksum)
        val hex = java.lang.Long.toHexString(checksum)

        if (prev != null && prev.checksum != checksum) {
            repeat(2) {
                Discord.sendImportantMessage(":exclamation::exclamation::exclamation: New *app.js* fetch | `$hex` | " +
                        "`$signature` **seems to" +
                        " be " +
                        "different from before**. @here")
            }
        } else {
            Discord.sendMessage(":eyes:  New app.js fetch | `$hex` | `$signature`")
        }

        logger.info("Checksum is $checksum")

        transaction {
            Db.AppFetch.new {
                this.data = String(bytes)
                this.checksum = checksum
                this.signature = signature
                this.fetchDate = DateTime.now()
            }
        }
    }

    private fun fetchSite() {
        val prev = transaction {
            val one = Db.SiteFetch.all().orderBy(Db.SiteFetches.id to SortOrder.DESC).limit(1)
            if(one.empty()) null else one.toList()[0]
        }

        val site = siteFetcher.fetch()

        val doc = Jsoup.parse(site.toString(Charsets.UTF_8))

        val scripts = doc.select("script")
                .asSequence()
                .filter { it.hasAttr("src") }
                .map { it.attr("src") }
                .map { src -> src + " @ " + Checksum.get(URL(src).readBytes()) }
                .sorted()
                .joinToString("<br>\n")

        logger.info("Scripts:\n$scripts")

        val checksum = Checksum.get(scripts.toByteArray())
        val signature = generateSignature(checksum)
        val hex = java.lang.Long.toHexString(checksum)

        if (prev != null && prev.checksum != checksum) {
            repeat(2) {
                Discord.sendImportantMessage(":exclamation::exclamation::exclamation: New *site* fetch | `$hex` | " +
                        "`$signature` **seems to" +
                        " be " +
                        "different from before**. @here")
            }
        } else {
            Discord.sendMessage(":eyes: New site fetch | `$hex` | `$signature`")
        }

        logger.info("Checksum is $checksum")

        transaction {
            Db.SiteFetch.new {
                this.scripts = scripts
                this.checksum = checksum
                this.signature = signature
                this.fetchDate = DateTime.now()
            }
        }
    }

    @Scheduled(fixedRate = "3m", initialDelay = "10s")
    fun fetch() {
        fetchApp()
        fetchSite()
    }
}