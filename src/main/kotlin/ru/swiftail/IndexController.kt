package ru.swiftail

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.views.View

@Controller("/")
class IndexController {

    @Get("/")
    @Produces(MediaType.TEXT_HTML)
    @View("index")
    fun index(): HttpResponse<Any> {
        return HttpResponse.ok(mapOf<String,Any>())
    }
}