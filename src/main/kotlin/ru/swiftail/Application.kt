package ru.swiftail

import io.micronaut.runtime.Micronaut
import ru.swiftail.starset.Checksum
import java.util.*

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("ru.swiftail")
                .mainClass(Application.javaClass)
                .start()
    }
}