<#-- @ftlvariable name="sys" type="ru.swiftail.index.Sys" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Swee's starset monitor</title>
    <!-- You MUST include jQuery before Fomantic -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.7.7/dist/semantic.min.css">
    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.7.7/dist/semantic.min.js"></script>
    <style>
        .wrapper_container {
            width: 500px;
            margin: 100px auto 0;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="ui container wrapper_container">
        <h1 class="ui header">Swee.pw</h1>
        <h2>Looking for the <a href="/starset">starset monitor</a>? </h2>
    </div>
</div>
</body>
</html>