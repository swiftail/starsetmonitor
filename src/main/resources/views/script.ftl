<!DOCTYPE html>
<html lang="en">
<head>
    <title>Swee's starset monitor</title>
    <!-- You MUST include jQuery before Fomantic -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.7.7/dist/semantic.min.css">
    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.7.7/dist/semantic.min.js"></script>
    <style>
        .wrapper_container {
            width: 500px;
            margin: 100px auto 0;
        }
        textarea {
            font-family: monospace!important;
        }
    </style>
    <script>
        function beautify() {
            var btn = $('#beautify');
            btn.toggleClass('loading');
            btn.toggleClass('disabled');
            var area = $('#data-area');
            var data = area.val();
            var beautified = js_beautify(data);
            area.val(beautified);
            btn.toggleClass('loading');
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-beautify/1.10.1/beautify.js"></script>
</head>
<body>
<div class="wrapper">
    <div class="ui container wrapper_container">
        <h1>Script <small style="padding-left: 20px"><a href="/starset/raw?id=${ id }">raw</a></small></h1>
        <div class="ui left icon input" style="width: 100%">
            <#--noinspection HtmlFormInputWithoutLabel-->
            <textarea style="width: 100%; height: 400px" id="data-area">${data}</textarea>
            <i class="code icon"></i>
        </div>
        <div class="ui primary fluid button" style="margin-top: 20px" onclick="beautify()" id="beautify">Beautify</div>
    </div>
</div>
</body>
</html>