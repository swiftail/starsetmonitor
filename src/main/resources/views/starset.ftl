<#-- @ftlvariable name="appFetches" type="java.util.List<ru.swiftail.starset.db.DTOFetch>" -->
<#-- @ftlvariable name="siteFetches" type="java.util.List<ru.swiftail.starset.db.DTOSiteFetch>" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Swee's starset monitor</title>
    <!-- You MUST include jQuery before Fomantic -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.7.7/dist/semantic.min.css">
    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.7.7/dist/semantic.min.js"></script>
    <style>
        .wrapper_container {
            width: 500px;
            margin: 100px auto 0;
        }
    </style>
    <script>
        function diffWith(id) {
            var id2 = prompt("Enter id to diff with");
            if (!id2 || isNaN(id2)) {
                return
            }
            window.location.href = '/starset/diff?id1=' + id + '&id2=' + id2
        }
    </script>
</head>
<body>
<div class="wrapper">
    <div class="ui container wrapper_container">
        <h1 class="ui header">Starset monitor</h1>
        <h2>App.js</h2>
        <table class="ui single line striped table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Time UTC</th>
                <th>Signature</th>
                <th>Checksum</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>

            <#assign
            lastChecksum = 0
            before = 0
            i = 0
            startDate = ""
            endDate = ""
            lastRenderedWasOk = false
            lastUniqueId = 0
            >

            <#list appFetches as data>
                <#assign
                i++
                sameChecksum = lastChecksum == data.checksum
                last = i == siteFetches?size
                >

                <#if sameChecksum && !last>
                    <#if lastRenderedWasOk>
                        <#assign startDate = data.formatFetchDate()>
                    </#if>
                    <#assign
                    endDate = data.formatFetchDate()
                    before++
                    lastRenderedWasOk = false
                    >
                <#else>
                    <#assign lastRenderedWasOk = true>
                    <#if before != 0>
                        <tr style="text-align: center">
                            <td colspan="5">
                                <div class="ui green horizontal label">${ data.formatChecksum(lastChecksum) }</div>
                                Same as before for ${ before } times (${ startDate!"null" } - ${ endDate!"null" })
                            </td>
                        </tr>
                        <#assign before = 0>
                    </#if>
                    <tr <#if last> class="blue" </#if>>
                        <td>
                            ${ data.id }
                        </td>
                        <td>
                            ${ data.formatFetchDate(data.fetchDate) }
                        </td>
                        <td>
                            <div class="ui basic label">
                                ${ data.signature }
                            </div>
                        </td>
                        <td>
                            <div class="ui basic label">
                                ${data.formatChecksum(data.checksum)}
                            </div>
                        </td>
                        <td>
                            <a class="ui compact small green button" href="/starset/script?id=${ data.id }">
                                View
                            </a>
                            <a class="ui compact small green button" href="/starset/raw?id=${ data.id }">
                                Raw
                            </a>
                            <#if lastUniqueId != 0>
                                <a class="ui compact small green button" href="/starset/diff?id1=${ data.id }&id2=${
                                lastUniqueId }">
                                    Diff with previous
                                </a>
                            </#if>
                            <a class="ui compact small basic green button diff-with"
                               onclick="diffWith(${ data.id })">Diff
                                with...</a>
                        </td>
                    </tr>
                    <#assign lastUniqueId = data.id.value>
                </#if>
                <#assign lastChecksum = data.checksum>
            </#list>
            </tbody>
        </table>
        <div class="ui divider" style="margin: 20px 0"></div>
        <h2>Site scripts</h2>
        <table class="ui single line striped table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Time UTC</th>
                <th>Signature</th>
                <th>Checksum</th>
                <th>Scripts</th>
            </tr>
            </thead>
            <tbody>

            <#assign
            lastChecksum = 0
            before = 0
            i = 0
            startDate = ""
            endDate = ""
            lastRenderedWasOk = false
            lastUniqueId = 0
            >

            <#list siteFetches as data>
                <#assign
                i++
                sameChecksum = lastChecksum == data.checksum
                last = i == siteFetches?size
                >

                <#if sameChecksum && !last>
                    <#if lastRenderedWasOk>
                        <#assign startDate = data.formatFetchDate()>
                    </#if>
                    <#assign
                    endDate = data.formatFetchDate()
                    before++
                    lastRenderedWasOk = false
                    >
                <#else>
                    <#assign lastRenderedWasOk = true>
                    <#if before != 0>
                        <tr style="text-align: center">
                            <td colspan="5">
                                <div class="ui orange horizontal label">${ data.formatChecksum(lastChecksum) }</div>
                                Same as before for ${ before } times (${ startDate!"null" } - ${ endDate!"null" })
                            </td>
                        </tr>
                        <#assign before = 0>
                    </#if>
                    <tr <#if last> class="blue" </#if>>
                        <td>
                            ${ data.id }
                        </td>
                        <td>
                            ${ data.formatFetchDate(data.fetchDate) }
                        </td>
                        <td>
                            <div class="ui basic label">
                                ${ data.signature }
                            </div>
                        </td>
                        <td>
                            <div class="ui basic label">
                                ${data.formatChecksum(data.checksum)}
                            </div>
                        </td>
                        <td>
                            <#assign buttonId = "btn-site-fetch"+data.id>
                            <#assign modalId = "mod-site-fetch"+data.id>
                            <a class="ui compact small orange button" id="${buttonId}">
                                View
                            </a>
                            <div class="ui modal" id="${modalId}">
                                <div class="ui segment">
                                    <p style="font-family: monospace!important;">
                                        ${ data.content }
                                    </p>
                                </div>
                            </div>
                            <script>
                                $('#${buttonId}')
                                    .on('click', function () {
                                        $('#${modalId}')
                                            .modal('show')
                                        ;
                                    })
                                ;
                            </script>
                        </td>
                    </tr>
                    <#assign lastUniqueId = data.id.value>
                </#if>
                <#assign lastChecksum = data.checksum>
            </#list>
            </tbody>
        </table>
        <div class="ui divider" style="margin: 20px 0"></div>
        <div class="footer" style="color: #999999">
            <p>Version 0.7</p>
            <p>Made by swee</p>
            <p><b>@danceslikesnake I LOVE YOU</b></p>
            <p><b>Dustin if you are here please marry me <3</b></p>
        </div>
    </div>
</div>
</body>
</html>